set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

subdirs(SxGeoParser)

cmake_minimum_required(VERSION 2.6)
cmake_policy(SET CMP0015 NEW)

#---------------------------------------------------

project(SxGeoParserTest CXX)
message ("${PROJECT_NAME} processing running...")
set(THIS_TARGET ${PROJECT_NAME})

#---------------------------------------------------

string(APPEND CMAKE_CXX_FLAGS " -std=gnu++14")

string(APPEND CMAKE_CXX_FLAGS " -DBOOST_LOG_DYN_LINK")

include_directories(
  ${PROJECT_SOURCE_DIR}
  ./SxGeoParser
)

link_directories(
  ${CMAKE_BUILD_DIRECTORY}/lib
)

set(HEADERS
)

set(SOURCES
  main.cpp
)

set(THIS_LINK_LIBS
  boost_program_options
#  boost_log

  SxGeoParser
)

add_executable(${THIS_TARGET} ${HEADERS} ${SOURCES})
target_link_libraries(${THIS_TARGET} ${THIS_LINK_LIBS})

#---------------------------------------------------
message ("${PROJECT_NAME} processed")
